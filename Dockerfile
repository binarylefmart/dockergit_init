FROM python:buster

RUN apt-get update -y && apt-get upgrade -y && apt-get dist-upgrade -y
RUN apt-get install -y git unzip wget
RUN wget https://gist.github.com/ttwthomas/bcfc524e0328343c6e70d0ac93f4ef3e/archive/a7b8c232554c58dddb06dd7a06025a45f92b0e66.zip -O site.zip
RUN unzip site.zip -d /tmp/

WORKDIR /tmp/bcfc524e0328343c6e70d0ac93f4ef3e-a7b8c232554c58dddb06dd7a06025a45f92b0e66

ENTRYPOINT ["python","-m","http.server","7777"]

EXPOSE 7777
